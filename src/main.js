import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './store'

import "@assets/sass/paper-dashboard.scss";
import "@assets/css/themify-icons.css"

import BootstrapVue from 'bootstrap-vue';
Vue.use(BootstrapVue);
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import { directive as vClickOutside } from "vue-clickaway";
Vue.directive("click-outside", vClickOutside);


Vue.use(VueRouter);

// auto-reconnect
const axios = require('axios');

// search into localStorage if we get any session data, and auto-relog if it's possible
const token = localStorage.getItem('user-token')
if (token) {
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
}   


const router = new VueRouter(require('./router'));
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('user') === null) {
      console.log('redirect to login ...')
      next({
        path: '/login',
        query: {
          redirect: to.fullPath,
        },
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

new Vue({
  render: function(h) {
    const App = require('./App.vue').default;
    return h(App);
  },
  store,
  router     
}).$mount('#app')
