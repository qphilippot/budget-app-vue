import TopNavbar from "../top-navbar";
import ContentFooter from "@layouts/content-footer";
import DashboardContent from "@layouts/content-dashboard";
import MobileMenu from "@layouts/mobile-menu";
import Sidebar from "@components/sidebar";
import SidebarLink from "@components/sidebar/sidebar.link";
import DropdownComponent from '@components/drop-down';

export default {
  components: {
    TopNavbar,
    ContentFooter,
    DashboardContent,
    MobileMenu,
    'dropdown': DropdownComponent,
    Sidebar,
    SidebarLink
  },
  methods: {
    toggleSidebar() {
      this.$store.commit('dashboard/toggleSidebar');
    }
  }
};
