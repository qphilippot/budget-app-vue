import DropdownComponent from '@components/drop-down';

export default {
  name: 'top-navbar',

  components: {
    'dropdown': DropdownComponent
  },

  computed: {
    routeName() {
      console.log(this.$route);
      const { name } = this.$route;
      return this.capitalizeFirstLetter(name);
    },

    isSidebarOpen() {
      return this.$store.getters['dashboard/isSidebarOpen'];
    }
  },

  data() {
    return {
      activeNotifications: false
    };
  },
  methods: {
    capitalizeFirstLetter(string = '') {
      return string.charAt(0).toUpperCase() + string.slice(1);
    },

    toggleNotificationDropDown() {
      this.activeNotifications = !this.activeNotifications;
    },
    
    closeDropDown() {
      this.activeNotifications = false;
    },
    
    toggleSidebar() {
      this.$store.commit('dashboard/toggleSidebar');
    },
    
    hideSidebar() {
      this.$store.commit('dashboard/hideSidebar');
    }
  }
};
