const OperationsList = require('@components/operations-list.controller').default;
const SignUp = require('../pages/signup').default;
const LoginComponent = require('../pages/login').default;
const RegisterComponent = require('../pages/signup').default;
const LogoutComponent = require('../components/logout').default;
const Layout = require('@layouts/dashboard').default;
// import Layout from '@layouts/dashboard/index.vue';

const Dashboard = require('@pages/dashboard').default;

module.exports = {
    mode: 'history',
    routes: [
        { 
          path: '/operations', 
          // component: require('./components/operations-list.controller') 
          component: OperationsList,
          meta: {
            requireAuth: true
          }
        },
        
        { 
          path: '/register', 
        // component: require('./pages/signup') 
          component: SignUp 
        },

        { 
          path: '/login', 
          // component: require('./pages/signup') 
          component: LoginComponent 
        },

        { 
          path: '/register', 
          // component: require('./pages/signup') 
          component: RegisterComponent 
        },

        { 
          path: '/logout', 
          // component: require('./pages/signup') 
          component: LogoutComponent 
        },

        { 
          path: '/auth/google/callback', 
          // component: require('./pages/signup') 
          component: Layout 
        },

        { 
            path: '/', 
            component: Layout,
            redirect: "/dashboard",
  
            children: [
              {
                path: "dashboard",
                name: "dashboard",
                component: Dashboard,
                meta: { requiresAuth: true}
              },
            //   {
            //     path: "stats",
            //     name: "stats",
            //     component: UserProfile
            //   },
            //   {
            //     path: "notifications",
            //     name: "notifications",
            //     component: Notifications
            //   },
            //   {
            //     path: "icons",
            //     name: "icons",
            //     component: Icons
            //   },
            //   {
            //     path: "maps",
            //     name: "maps",
            //     component: Maps
            //   },
            //   {
            //     path: "typography",
            //     name: "typography",
            //     component: Typography
            //   },
            //   {
            //     path: "table-list",
            //     name: "table-list",
            //     component: TableList
            //   }
            ]
        },

        { 
            path: '*', 
            redirect: '/dashboard' 
        },
    ]
};