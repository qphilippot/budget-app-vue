import StatsCard from "@components/stats-card";
import ChartCard  from "@components/chart-card";
import BudgetCard  from "@components/budget-card";

import Chartist from 'chartist';
import axios from 'axios';

export default {
  name: 'dashboard',
  components: {
    BudgetCard,
    StatsCard,
    ChartCard
  },
  /**
   * Chart data used to render stats, charts. 
   * Should be replaced with server data
   */
  data() {
    return {
      charge: 100,
      timer: '',
      usersChart: {
        data: {
          labels: [
            "9:00AM",
            "12:00AM",
            "3:00PM",
            "6:00PM",
            "9:00PM",
            "12:00PM",
            "3:00AM",
            "6:00AM"
          ],
          series: [
            [287, 385, 490, 562, 594, 626, 698, 895, 952],
            [67, 152, 193, 240, 387, 435, 535, 642, 744],
            [23, 113, 67, 108, 190, 239, 307, 410, 410]
          ]
        },
        options: {
          low: 0,
          high: 1000,
          showArea: true,
          height: "245px",
          axisX: {
            showGrid: false
          },
          lineSmooth: Chartist.Interpolation.simple({
            divisor: 3
          }),
          showLine: true,
          showPoint: false
        }
      },

      activityChart: {
        data: {
          labels: [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "Mai",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"
          ],
          series: [
            [542, 543, 520, 680, 653, 753, 326, 434, 568, 610, 756, 895],
            [230, 293, 380, 480, 503, 553, 600, 664, 698, 710, 736, 795]
          ]
        },
        options: {
          seriesBarDistance: 10,
          axisX: {
            showGrid: false
          },
          height: "245px"
        }
      },
      preferencesChart: {
        data: {
          labels: ["62%", "32%", "6%"],
          series: [62, 32, 6]
        },
        options: {}
      }
    };
  },

  methods: {
    updateCharges() {
      console.log("update charges")
      this.$store.dispatch('operations/refreshCharges').then(() => {
        console.log(this.charges);
      });
    }
  },

  created () {
    // search into localStorage if we get any session data, and auto-relog if it's possible
    const token = localStorage.getItem('user-token')
    if (token) {
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    }
    console.log('token', token,  axios.defaults.headers.common['Authorization']);
    this.updateCharges();
    // this.timer = setInterval(() => {
    //   console.log("tick")
    //   this.updateCharges();
    // }, 60000)
  },

  beforeDestroy () {
    // clearInterval(this.timer)
  },

  computed: {
    charges() {
      return this.$store.getters['operations/charges'];
    },

    revenus() {
      return this.$store.getters['operations/revenus'];
    },

    budgetCard() {
      return {
        loose: -100,
        earn: 80
      }
    },

    statsCards() {
      return [
        {
          type: "warning",
          icon: "ti-server",
          title: 'Frais mensuel',
          value: this.charges + '€',
          footerText: "Updated now",
          footerIcon: "ti-reload"
        },

        {
          type: "success",
          icon: "ti-wallet",
          title: "Revenue",
          value: this.revenus + '€',
          footerText: "Last day",
          footerIcon: "ti-calendar"
        },
        {
          type: "danger",
          icon: "ti-pulse",
          title: "Errors",
          value: "23",
          footerText: "In the last hour",
          footerIcon: "ti-timer"
        },
        {
          type: "info",
          icon: "ti-twitter-alt",
          title: "Followers",
          value: "+45",
          footerText: "Updated now",
          footerIcon: "ti-reload"
        }
      ]
    }
  }
};
