import { mapMutations } from 'vuex';


export default {
  name: 'login',
  data() {
    return {
      email: '',
      password: '',
      loading: false
    }
  },
  computed: {
    isLoading() {
      return this.$store.getters['auth/isLoading'];
    }
  },
  
  methods: {
    async handleSubmit() {
      try {
        this.loading = true
       
        this.$store.dispatch('auth/login', {
          email: this.email,
          password: this.password
        }).then(() => {
          console.log('login end', this.$store.getters['auth/username'])
          this.loading = false
          this.$router.push('/');
        });   
      } 
      
      catch (err) {
        this.loading = false
        alert(err.message || 'An error occurred.')
      }
    },
  },

  mounted() {
    // axios.get(`${apiUrl}/connect/google`, {
    //   headers: {
    //     'Access-Control-Allow-Origin': '*',
    //     'Access-Control-Allow-Headers': '*',
    //   }
    // })
    // .then(response => {
    //   console.log(response);

    //   this.loading = false
    //   this.setUser(response.data.user);
    //   this.$router.go(-1)
    // });

   
  }
}