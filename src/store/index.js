 
import Vue from 'vue';
import Vuex from 'vuex';
import auth from './modules/auth';
import operations from './modules/operations';
import dashboard from './modules/dashboard';

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    auth,
    operations,
    dashboard
  },
  
  strict: debug
})