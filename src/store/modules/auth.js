import Cookies from 'js-cookie';
import strapiProvider from '../../api/strapi';
import axios from 'axios';

export default {
  namespaced: true,
  state: () => {
    return {
      token: localStorage.getItem('user-token') || '',
      status: '',
      user: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : {}
    }
  },

  mutations: {
    onLoginRequest(state) {
      state.status = 'loading';
    },

    onLoginFail(state, loginData) {
      state.status = 'error';
      Cookies.set('user', null);
      localStorage.removeItem('user-token');
      localStorage.removeItem('user');
      delete axios.defaults.headers.common['Authorization'];
    },

    onLoginSuccess(state, loginData) {
      console.log('onLoginSuccess', loginData)
      // store user data in order to display some account data
      const user = loginData.user;
      state.user = user;
      Cookies.set('user', user);
      localStorage.setItem('user', JSON.stringify(user));

      // store token
      const token = loginData.token;
      localStorage.setItem('user-token', token);
      state.token = token;
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;

      // uptade status
      state.status = 'success';
    },
  
    onLogout(state) {
      state.user = null;
      localStorage.removeItem('user-token');
      localStorage.removeItem('user');
      Cookies.set('user', null);
      delete axios.defaults.headers.common['Authorization'];
    }
  },
  
  actions: {
    login({ commit }, authData) {
      return new Promise(resolve => {
        commit('onLoginRequest');
        strapiProvider.login(authData.email, authData.password).then(
          loginData => {
            console.log('loginData', loginData);
            commit('onLoginSuccess', loginData);
            resolve();
          },

          error => {
            commit('onLoginFail', error);
            resolve();
          }
        )   
      });
    },

    logout({ commit }) {
      return new Promise((resolve) => {
        commit('onLogout');
        resolve();
      });
    },

    register({ commit }, authData) {
      return new Promise((resolve) => {
        strapiProvider.register(authData).then(registerData => {
          commit('onLoginSuccess', registerData);
          resolve();
        });
      });
    }
  },

  getters: {
    username: state => {
      console.log('user', state.user)
      if (state.user && state.user.username) {
        return  state.user.username;
      }

      else {
        return null;
      } 
    },

    isAuthenticated: state => !!state.token,
    isLoading: state => state.status === 'loading'
  }
};
