const apiUrl = process.env.API_URL || 'http://localhost:1337'
import axios from 'axios';

export default {
    namespaced: true,

    state: {
        all: [],
        lastLoaded: null
    },

    getters: {
        operations(state) {
            return state.all;
        },

        charges(state) {
            const op = state.all;
            if (op.length > 0) {
                console.table(op)
                const charges = op.reduce((acc, value) => {
                    if (!Array.isArray(value.tags) || value.tags.indexOf('charge') < 0) {
                        return acc;
                    }

                    else {
                        return acc + value.amount;
                    }
                }, 0);

                console.log('ch', charges);
                return Math.abs(charges.toFixed(2));
            }

            else {
                return 0;
            }
        },

        revenus(state) {
            const op = state.all;
            if (op.length > 0) {
                console.table(op)
                const charges = op.reduce((acc, value) => {
                    if (!Array.isArray(value.tags) || value.tags.indexOf('revenu') < 0) {
                        return acc;
                    }

                    else {
                        return acc + value.amount;
                    }
                }, 0);

                console.log('ch', charges);
                return Math.abs(charges.toFixed(2));
            }

            else {
                return 0;
            }
        }
    },
    

    actions: {
        getAllOperations({ commit }) {
            axios.get(`${apiUrl}/operations/my-operations`).then(
                response => {
                    commit('setOperations', response.data);
                }
            );
        },

        refreshCharges({ state, commit, dispatch }) {
            if (
                state.lastLoaded === null ||
                state.lastLoaded + 1000 < Date.now()
            ) {
                dispatch('getAllOperations');
                commit('lastRefreshIsNow');
            }
        }
    },
    
    mutations: {
        setOperations(state, operations) {
            console.log('operation =', operations)
            state.all = operations;
        },

        lastRefreshIsNow(state) {
            state.lastLoaded = Date.now();
        }
    }
}

