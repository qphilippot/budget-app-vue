export default {
  namespaced: true,
  state: {
    ui: {
      sidebarOpen: true
    },

    charges:  (Math.random() * 100).toFixed(2)
  },

  actions: {
    refreshCharges({ state, commit, rootGetters, dispatch }) {
      return new Promise((resolve) => {
        dispatch('operations/getAllOperations', {}, { root: true }).then(() => {
          const charges = rootGetters['operations/operations'];
          console.log('list', charges);
          commit('setCharges', (Math.random() * 100).toFixed(2))
          resolve(state.charges);
        });
      });
    },
  },

  mutations: {
    showSidebar(state) {
      state.ui.sidebarOpen = true;
    },

    setCharges(state, value) {
      state.charges = value; 
    },

    hideSidebar(state) {
      state.ui.sidebarOpen = false;
    },

    toggleSidebar(state) {
      state.ui.sidebarOpen = !state.ui.sidebarOpen;
    }
  },

  getters: {
    isSidebarOpen: state => {
     return state.ui.sidebarOpen;
    },

    charges: state => {
      return state.charges;
     },
  }
};
