import axios from 'axios';

const provider = {
    apiUrl: process.env.API_URL || 'http://localhost:1337',

    async login(email, password) {
        const response = await axios.post(`${this.apiUrl}/auth/local`, {
            identifier: email, 
            password: password
        });

        return {
            user: response.data.user,
            token: response.data.jwt
        };
    },

    async register({ username, email, password }) {
        const response = await axios.post(`${this.apiUrl}/auth/local/register`, {
            username,
            email,
            password
        });

        return {
            user: response.data.user,
            token: response.data.jwt
        };
    }
};

export default provider;