import { mapState } from 'vuex';

export default {
  name: 'OperationsList',
  components: {},
  props: [],

  computed: {
   ...mapState({
     operations: state => state.operations.all
   })
  },

  created() {
    this.$store.dispatch('operations/getAllOperations');
  },

  methods: {

  }
}