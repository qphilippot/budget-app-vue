export default {
  
  computed: {
    username() {
      return this.$store.getters['auth/username'];
    }
  },

  methods: {
    logout() {
      this.$store.commit('auth/logout');
      this.$router.push('/');
    }
  }
}