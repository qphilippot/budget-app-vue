import MovingArrow from "./sidebar.arrow";
import SidebarLink from "./sidebar.link";

export default {
  name: 'sidebar',
  props: {
    title: {
      type: String,
      default: "Budget Dashboard"
    },

    sidebarLinks: {
      type: Array,
      default: () => []
    },
    
    autoClose: {
      type: Boolean,
      default: true
    }
  },

  provide() {
    return {
      autoClose: this.autoClose,
      addLink: this.addLink,
      removeLink: this.removeLink
    };
  },

  components: {
    MovingArrow,
    SidebarLink
  },

  computed: {
    /**
     * Styles to animate the arrow near the current active sidebar link
     * @returns {{transform: string}}
     */
    arrowMovePx() {
      return this.linkHeight * this.activeLinkIndex;
    }
  },

  data() {
    return {
      linkHeight: 65,
      activeLinkIndex: 0,
      windowWidth: 0,
      isWindows: false,
      hasAutoHeight: false,
      links: []
    };
  },

  methods: {
    findActiveLink() {
      this.links.forEach((link, index) => {
        if (link.isActive()) {
          this.activeLinkIndex = index;
        }
      });
    },

    addLink(link) {
      const index = this.$slots.links.indexOf(link.$vnode);
      this.links.splice(index, 0, link);
    },

    removeLink(link) {
      const index = this.links.indexOf(link);
      if (index > -1) {
        this.links.splice(index, 1);
      }
    }
  },

  mounted() {
    this.$watch("$route", this.findActiveLink, {
      immediate: true
    });
  }
};

