const path = require('path');

module.exports = {
  chainWebpack: (config) => {
    config.resolve.alias
      .set('@components', path.resolve(__dirname, './src/components'))
      .set('@layouts', path.resolve(__dirname, './src/layouts'))
      .set('@pages', path.resolve(__dirname, './src/pages'))
      .set('@store', path.resolve(__dirname, './src/store'))
      .set('@assets', path.resolve(__dirname, './src/assets'));
  }
};
